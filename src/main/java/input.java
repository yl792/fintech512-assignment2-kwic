import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.TreeMap;

public class input {
    public List<String> ignore;
    public List<String> titles;
    public List<List<String>> words;
    public TreeMap<String,ArrayList<String>> map;

    input(String s){
       String[] splited = s.split("::");
        this.ignore = new ArrayList<String>();
        this.titles = new ArrayList<String>();
        this.words = new ArrayList<List<String>>();
        for (String s_ignore: splited[0].split("\n")){
            this.ignore.add(s_ignore);
        }

        for(String s_titles: splited[1].split("\n")){
            if(s_titles!="") {
                this.titles.add(s_titles);
            }
        }

        for(String i : titles){
            ArrayList<String> temp = new ArrayList<String>();
            for(String s_words: i.split(" ")){
                temp.add(s_words);
            }
            this.words.add(temp);
        }
    }

    public String get_ignore(){
        String string_ignore = "";
        for(String i : ignore){
            string_ignore += i + "\n";
        }
        return string_ignore;
    }
    public String get_titles(){
        String string_titles = "";
        for(String i : titles){
            string_titles += i + "\n";
        }
        return string_titles;
    }

    public void find_keywords(){
        this.map = new TreeMap<String,ArrayList<String>>();
        for(int i=0;i<words.size();i++){
            for(int j=0;j<words.get(i).size();j++){
                if(!ignore.contains(words.get(i).get(j).toLowerCase())){
                    if(!map.containsKey(words.get(i).get(j).toLowerCase())){
                        ArrayList<String> temp = new ArrayList<String>();
                        String p_line="";
                        for(int k=0; k<words.get(i).size();k++){
                            if(k!=j){
                                p_line+=words.get(i).get(k).toLowerCase();
                            }
                            else if(k==j){
                                p_line+=words.get(i).get(k).toUpperCase();
                            }
                            if(k!=words.get(i).size()-1){
                                p_line+=" ";
                            }
                        }
                        temp.add(p_line);
                        map.put(words.get(i).get(j).toLowerCase(),temp);
                    }else{
                        ArrayList<String> temp = new ArrayList<String>();
                        temp = map.get(words.get(i).get(j).toLowerCase());
                        String p_line="";
                        for(int k=0; k<words.get(i).size();k++){
                            if(k!=j){
                                p_line+=words.get(i).get(k).toLowerCase();
                            }
                            else if(k==j){
                                p_line+=words.get(i).get(k).toUpperCase();
                            }
                            if(k!=words.get(i).size()-1){
                                p_line+=" ";
                            }
                        }
                        temp.add(p_line);
                        map.put(words.get(i).get(j).toLowerCase(),temp);
                    }
                }
            }
        }
    }

    public String return_output(){
        String result="";
        for(ArrayList<String> line:map.values()){
            for(int i=0;i<line.size();i++){
                result+=line.get(i);
                result+="\n";
            }
        }
        result=result.substring(0,result.length()-1);
        return result;
    }
}
